This is a small scripted tool to help set up an run multiple OpenERP based projects and develop addons for them.
It's mainly based on Buildout and Anybox recipe for OpenERP as well as few others.

Running
=======
To use it - just run ``make``. It will build up example project for you. You might encounter error::

  Traceback (most recent call last):
    File "bootstrap.py", line 108, in <module>
      pkg_resources.Requirement.parse('distribute')).location
  AttributeError: 'NoneType' object has no attribute 'location'
  make: *** [bootstrap] Error

If so, run ``./virtual/bin/pip install distribute --upgrade``

Optionally if you know that you only have changed just the buildout config, you can run ``make buildout``, this will save few commands, but realistically this is meant for scripting only.

After successful build you'll find executables in ``virtual/bin/*``.

Also you'll find ``local.cfg`` appeared in oede root folder. This is your personal config that is ignored by git. It might be a good idea to back it up.

Structure
=======
Everything are set in few set locations. In ``./virtual/bin/*`` you'll find executables. In ``etc/`` you'll find generated config files for each of the project. In ``projects/`` you'll find project folders with server and addons inside each one of them.

Projects
======
Projects can be described in few locations. It is possible to place them in separate git repository and just use those by editing ``local.cfg``::

  [buildout]
  extends =

Also projects can be described in the very same ``local.cfg``, or just parts of the project changed to meet the local configuration as provided in the example configs seen just after the first build.

The way they should be described should meet the sample in ``example/project.cfg``. Just a few notes on that:

*  Everything that should be changed for openerp config can be changed by addind ``options.*`` field to the project configuration just as in example - ``options.xmlrpc_port``.
*  Also project dir should exist before building there fore you will see ``[dirs]`` section at the top of the ``project.cfg``. This handles required folder creation.
*  ``[initdb]`` section is just a cmd command list used to create postgresql db. One point is that it cannot return exit code 1. There fore you see attached ``|| echo ...``.
*  Project version field path does not contain ``projects`` path, while addons do. This means that addons can be placed in another folder and shared if wanted to.

More on project configuration can be found here: http://docs.anybox.fr/anybox.recipe.openerp/1.8.3/configuration.html
