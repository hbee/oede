# virtualenv related stuff
VIRTUALENV_PROMPT = oede

# python executables
PYTHON := $(shell if [ ! -z "`python --version 2>&1 | grep 'Python 2'`" ] ; then echo python; else echo python2; fi)
BIN = virtual/bin
VIRTUAL_PYTHON = $(BIN)/python
PIP = $(BIN)/pip

all: buildout

virtual:
	virtualenv --unzip-setuptools --prompt='$(VIRTUALENV_PROMPT)::' --python=$(PYTHON) virtual \
	|| \
	virtualenv --unzip-setuptools --python=$(PYTHON) virtual

config:
	test -s local.cfg || cp example/example.local.cfg local.cfg

bootstrap: virtual
	test -s $(BIN)/buildout || $(VIRTUAL_PYTHON) bootstrap.py -c local.cfg

buildout: config bootstrap
	$(BIN)/buildout -N -c local.cfg

clean-pyc:
	find . -name "*.pyc" -exec rm -rf '{}' \;

clean: clean-pyc
	rm -rf virtual
	rm -rf dist
	find . -name "*.egg-info" -exec rm -rf '{}' \;
	find . -name "*.pyc" -exec rm -rf '{}' \;
